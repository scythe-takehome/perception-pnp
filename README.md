# Overview: 

Create a C++ or Rust program to refine an initial pose estimate given a set of 3D world point to 2D image correspondences. The program should use the Gauss-Newton (GN) algorithm to minimize reprojection error over the set of correspondences. In the interest of time, the program only needs to refine the translation components of the pose, and it does not need to be robust to outliers. However, feel free to add orientation refinement and outlier robustness if you want to. If you don’t, we do ask for a short written description of how you would add these features (see the What to send back section below).

The Gauss-Newton update should be implemented yourself (don't use an external solver that does it internally), but you can use a math library like Eigen or nalgebra for matrix calculations and solving linear systems.

You don’t need to spend a TON of time on this challenge. If you are spending 4+ hours on this feel free to send us the code where it is at along with what you are blocked on and what you would have done if you had more time. 

# Input: 

The camera intrinsics, initial pose guess, and 3D <-> 2D correspondences are given in an input file. The program should take this file as an argument. 

The format of the file is as follows:

* The first line contains the camera intrinsics (fx,fy,cx,cy,width,height) for a pinhole camera with no distortion. 
* The second line contains the initial pose guess: qw,qx,qy,qz (rotation R as a quaternion) and tx,ty,tz (translation T). Together these define the worldFromImage [R T] initial pose. 
* The remaining lines contain 3D <-> 2D point correspondences. wx,wy,wz defines a 3D point in the world Wxyz. ix,iy defines a 2D point in the image Ixy.  Altogether, Ixy = K * worldFromImage.inverse() * Wxyz.
* Sample layout:
  * fx,fy,cx,cy,width,height
  * qw,qx,qy,qz,tx,ty,tz
  * wx,wy,wz,ix,iy
  * wx,wy,wz,ix,iy
  * wx,wy,wz,ix,iy
  * ...

Example input files are available in the **sample_inputs** directory. The **pnp_input_descriptions** file explains the difference between the examples. Some have outliers, some have translation and rotation offset from the true pose, etc… The most basic is **pnp_input_simple**.
    
# Output:

After each GN iteration the program should output the current pose estimate and objective function value. After minimization is complete the program should output the final worldFromImage estimate, as well as the reprojection error RMSE for the initial pose and the final pose.
> rmse = sqrt(sum_over_points((px-ix)^2 + (py-iy)^2)) / num_points)

Where, px/py is the result of  Ixy = K * worldFromImage.inverse() * Wxyz, and ix/iy are the image coordinates from the input file.

# What to send back:

1. Source code of your program and instructions for compiling and running it.
2. Answers to the following questions:
   1. If your program does not refine the orientation, how would you modify it to do so? What representation would you use for the orientation and how would you update it during GN?
   2. If your program is not robust to outliers, how would you modify it to make it robust?
   3. What are the pros/cons of using Gauss-Newton to refine the pose? What other algorithm(s) might you use?

# How to send it back?

* Send us a .tar or .zip file containing all the above in the same thread where you scheduled all our interviews.

# If anything is unclear about what you need to do please reach out and we will clarify.
